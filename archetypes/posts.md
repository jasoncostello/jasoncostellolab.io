---
date: '{{ .Date }}'
draft: true
title: '{{ replace .File.ContentBaseName `-` ` ` | title }}'

[//]: # (slug = 'hugo')
[//]: # (tags = ['Development', 'Go', 'fast', 'Blogging'])
---
